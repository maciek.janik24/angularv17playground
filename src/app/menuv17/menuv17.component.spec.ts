import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Menuv17Component } from './menuv17.component';

describe('Menuv17Component', () => {
  let component: Menuv17Component;
  let fixture: ComponentFixture<Menuv17Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [Menuv17Component],
    })
      .compileComponents();

    fixture = TestBed.createComponent(Menuv17Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
