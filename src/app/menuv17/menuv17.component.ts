import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menuv17',
  standalone: true,
  imports: [CommonModule, MatMenuModule, MatButtonModule, MatChipsModule, MatIconModule],
  templateUrl: './menuv17.component.html',
  styleUrl: './menuv17.component.scss',
})
export class Menuv17Component {
  constructor(private router: Router) {
  }

  navigate(path: string) {
    this.router.navigate([`${path}`]);
  }
}
