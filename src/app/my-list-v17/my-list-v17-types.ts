export type TodoListItem = {
  name: string;
  creationDate: string;
  description?: string;
  isDone?:boolean;
};
