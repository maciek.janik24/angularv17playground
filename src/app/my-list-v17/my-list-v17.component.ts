import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoListItem } from './my-list-v17-types';
import { MatListModule } from '@angular/material/list';

@Component({
  selector: 'app-my-list-v17',
  standalone: true,
  imports: [CommonModule, MatListModule],
  templateUrl: './my-list-v17.component.html',
  styleUrl: './my-list-v17.component.scss',
})
export class MyListV17Component implements OnInit {
  currentList: TodoListItem[] = [];

  newName: string = '';

  ngOnInit(): void {
    if (typeof localStorage !== 'undefined') {
      const listSesstion = localStorage.getItem('todolistv17');
      this.currentList = (JSON.parse(listSesstion != null ? listSesstion : '[]') as TodoListItem[]);
    } else {
      console.info('Service - Web Storage is not supported in this environment.');
    }
  }

  get isListExits(): boolean {
    return this.currentList?.length > 0;
  }

  add() {
    this.currentList = [
      ...this.currentList,
      {
        name: this.newName,
        creationDate: new Date().toString(),
      },
    ];

    if (typeof localStorage !== 'undefined') {
      localStorage.setItem('todolistv17', JSON.stringify(this.currentList));
      this.newName = '';
    } else {
      console.info('Service - Web Storage is not supported in this environment.');
    }
  }

  changeTitle($event: KeyboardEvent) {
    const newName = ($event.target as HTMLInputElement).value;
    this.newName = newName;
  }

  clearList() {
    localStorage.removeItem('todolistv17');
    this.currentList = [];
  }
}
