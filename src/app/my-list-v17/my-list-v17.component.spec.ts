import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyListV17Component } from './my-list-v17.component';

describe('MyListV17Component', () => {
  let component: MyListV17Component;
  let fixture: ComponentFixture<MyListV17Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MyListV17Component],
    })
      .compileComponents();

    fixture = TestBed.createComponent(MyListV17Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
