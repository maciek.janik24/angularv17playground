import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MyListV17Component } from './my-list-v17/my-list-v17.component';
import { SignalsComponent } from './signals/signals.component';

export const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'my-list', component: MyListV17Component },
  { path: 'signals', component: SignalsComponent },
  { path: '*', component: HomeComponent },
];
