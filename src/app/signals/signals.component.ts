import { Component, effect, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoListItem } from '../my-list-v17/my-list-v17-types';
import { MatListModule } from '@angular/material/list';

@Component({
  selector: 'app-signals',
  standalone: true,
  imports: [CommonModule, MatListModule],
  templateUrl: './signals.component.html',
  styleUrl: './signals.component.scss',
})
export class SignalsComponent {
  name = signal('');

  nameChangeEffect = effect(()=>{
    console.log('change: ', this.name());
  });

  listTodo = signal<TodoListItem[]>([{ name: 'init', creationDate: new Date().toString() }]);

  changeTitle($event: Event) {
    const newName = ($event.target as HTMLInputElement).value;
    this.name.set(newName);
  }

  AddNew() {
    this.listTodo.update((prev)=> [
      ...prev,
      {
        name: this.name(),
        creationDate: new Date().toString(),
      },
    ]);
    this.name.set('');
  }
}
